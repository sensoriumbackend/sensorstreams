<?php

namespace Sensorium\Sensorstreams;

use Illuminate\Support\Collection;

class Sensorstreams {

	public function __construct()
	{

	}

	// Fetch sensorstreams for a user (entity using trait HasSensorstreams)
	public function getSensorstreams($hasSensorstreams): Collection
	{
		return $hasSensorstreams->sensorstreams()->get();
	}

	// Returns the first queue in analyses (can be used for queueing data)
	public function getHeadQueues($hasSensorstreams)
	{
		$queues = $hasSensorstreams->sensorstreams()->get()->mapWithKeys(function($stream, $key) {
			return [$stream->input => $stream->analyses->queues[0]];
		});

		return $queues;
	}

	// Returns the tail queue in analyses (forwarded to analyses pipeline, spark can use it to divide results to n-th level results)
	public function getTailQueues($hasSensorstreams)
	{
		$queues = $hasSensorstreams->sensorstreams()->get()->mapToGroups(function($stream, $key) {
			return [$stream->input => implode('.', array_slice($stream->analyses->queues, 1))];
		});

		return $queues;
	}

	// Add sensorstream for user by name
	public function addSensorstream($hasSensorstreams, $streamName) 
	{
		$stream = Sensorstream::where('name', $streamName)->firstOrFail();
		$hasSensorstreams->sensorstreams()->syncWithoutDetaching($stream->id);
	}

	// Sync sensorstreams
	public function syncSensorstreams($hasSensorstreams, Collection $streams) 
	{
		$hasSensorstreams->sensorstreams()->syncWithoutDetaching($streams->pluck('id'));
	}
}

	
