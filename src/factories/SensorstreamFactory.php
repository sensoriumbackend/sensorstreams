<?php

use Faker\Generator as Faker;
use Sensorium\Sensorstreams\Sensorstream;

$factory->define(Sensorium\Sensorstreams\Sensorstream::class, function (Faker $faker) {
    return [
        'name' => 'rest',
        'analyses' => 'flow.rest',
        'input' => 'interbeats',
    ];
});