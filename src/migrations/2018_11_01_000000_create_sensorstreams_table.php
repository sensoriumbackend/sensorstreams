<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSensorstreamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sensorstreams', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('input');
            $table->jsonb('analyses');
        });

        Schema::create('sensorstream_user', function(Blueprint $table) {
            $table->integer('sensorstream_id')->unsigned();
            $table->foreign('sensorstream_id')->references('id')->on('sensorstreams')->onDelete('cascade');

            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });

        Schema::create('client_service_sensorstream', function(Blueprint $table) {
            $table->integer('sensorstream_id')->unsigned();
            $table->foreign('sensorstream_id')->references('id')->on('sensorstreams')->onDelete('cascade');

            $table->integer('client_service_id')->unsigned();
            $table->foreign('client_service_id')->references('id')->on('client_services')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasTable('sensorstream_user')) {
            Schema::table('sensorstream_user', function(Blueprint $table) {
                $table->dropForeign('sensorstream_user_sensorstream_id_foreign');
                $table->dropForeign('sensorstream_user_user_id_foreign');
            });
            Schema::drop('sensorstream_user');
        }

        if(Schema::hasTable('client_service_sensorstream')) {
            Schema::table('client_service_sensorstream', function(Blueprint $table) {
                $table->dropForeign('client_service_sensorstream_sensorstream_id_foreign');
                $table->dropForeign('client_service_sensorstream_client_service_id_foreign');
            });
            Schema::drop('client_service_sensorstream');
        }

        if(Schema::hasTable('sensorstreams')) {
            Schema::drop('sensorstreams');
        }
    }
}
