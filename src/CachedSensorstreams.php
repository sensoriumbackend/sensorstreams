<?php

namespace Sensorium\Sensorstreams;

use Illuminate\Support\Collection;
use Sensorium\Sensorstreams\Sensorstreams;
use Cache;

class CachedSensorstreams {

	public function __construct(Sensorstreams $service)
	{
		$this->service = $service;
	}

	// Fetch sensorstreams for a user (entity using trait HasSensorstreams)
	public function getSensorstreams($hasSensorstreams): Collection
	{
		return Cache::remember("sstream-col:". class_basename($hasSensorstreams). ":" .$hasSensorstreams->id, 60, function() use ($hasSensorstreams) {
            return $this->service->getSensorstreams($hasSensorstreams);
       	});
	}

	// Returns the first queue in analyses (can be used for queueing data)
	public function getHeadQueues($hasSensorstreams)
	{
		return Cache::remember("sstream-hd:". class_basename($hasSensorstreams). ":" .$hasSensorstreams->id, 60, function() use ($hasSensorstreams) {
            return $this->service->getHeadQueues($hasSensorstreams);
       	});
	}

	// Returns the tail queue in analyses (forwarded to analyses pipeline, spark can use it to divide results to n-th level results)
	public function getTailQueues($hasSensorstreams)
	{
		return Cache::remember("sstream-tl:". class_basename($hasSensorstreams). ":" .$hasSensorstreams->id, 60, function() use ($hasSensorstreams) {
            return $this->service->getTailQueues($hasSensorstreams);
       	});
	}

	// Add sensorstreams for user
	public function addSensorstreams($hasSensorstreams, $streamName) 
	{
		$stream = Sensorstream::where('name', $streamName)->firstOrFail();
		$hasSensorstreams->sensorstreams()->attach($stream->id);
	}
}

	
