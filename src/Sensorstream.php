<?php

namespace Sensorium\Sensorstreams;

use Illuminate\Database\Eloquent\Model;

class Sensorstream extends Model
{
	protected $table = 'sensorstreams';
	public $timestamps = false;

	protected $casts = [
        'analyses' => 'object' // defines consecutive queues for the input data
    ];

	public function users()
	{
		// uses helper function to fetch configured User model */
		return $this->belongsToMany(getModelForGuard(config('auth.defaults.guard')));
	}
}