<?php

namespace Sensorium\Sensorstreams;;

use Sensorium\Sensorstreams\Sensorstream;

/* This trait relates sensorstreams to users */
trait HasSensorstreams 
{
	public function sensorstreams()
    {
        return $this->belongsToMany(Sensorstream::class);
    }
}