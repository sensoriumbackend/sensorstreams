<?php

namespace Sensorium\Sensorstreams;

use Illuminate\Support\ServiceProvider;
use Sensorium\Sensorstreams\CachedSensorstreams;
use Sensorium\Sensorstreams\Sensorstreams;

class SensorstreamsServiceProvider extends ServiceProvider
{
	protected $defer = true;

    public function boot(): void
    {
        if(config('app.name') == 'Sensorium') {
        	// Only perform migrations at the master. Synapsis reads the same database but should not perform migrations
        	$this->loadMigrationsFrom(__DIR__.'/migrations');
        }
    }

    public function register(): void
    {
    	$this->app->singleton('Sensorstreams', CachedSensorstreams::class);
    	//$this->app->singleton('Sensorstreams', Sensorstreams::class);
    }

    /**
     *   Needed for deferred providers
     *   @return array provided services
     */
    public function provides()
    {
        return [
            'Sensorstreams',
        ];
    }

}
