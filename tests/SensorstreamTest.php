<?php

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Orchestra\Testbench\TestCase;
use Sensorium\Sensorstreams\Sensorstream;
use Sensorium\Sensorstreams\Sensorstreams;
use Sensorium\Sensorstreams\SensorstreamsServiceProvider;

class SensorstreamTest extends TestCase 
{
	public function setUp()
	{
		parent::setUp();

        $this->loadMigrationsFrom(__DIR__.'/../src/migrations');
        //$this->withFactories(__DIR__.'/../src/factories');
	}

	public function tearDown()
    {
    	try {
    		parent::tearDown();
    	}catch(\BadMethodCallException $e) {
    		// catch the exception that sqlite does not support foreign key drop...we just ignore it since the tables can just be cleared
    	}
    }

	protected function getPackageProviders($app)
    {
        return [
            SensorstreamsServiceProvider::class,
        ];
    }

	protected function getEnvironmentSetUp($app)
    {
        $app['config']->set('database.default', 'testbench');
        $app['config']->set('database.connections.testbench', [
            'driver'   => 'sqlite',
            'database' => ':memory:',
            'prefix'   => '',
        ]);
    }

    protected function getTablesNames()
    {
        return [
            'sensorstreams'
        ];
    }

    /** @test */
    public function it_can_migrate() 
    {
        foreach ($this->getTablesNames() as $table) {
            $this->assertTrue(Schema::hasTable($table), "The table [$table] not found in the database.");
        }
    } 

    /** @test */
    public function it_can_save_and_retrieve_analysis_for_user()
    {
        $stream = (new Sensorstream)->forceFill(['name' => 'rest', 'input' => 'interbeats', 'analyses' => ["queues" => "flow.rest"]]);
        $stream->save();
        $user = (new User)->forceFill(['id' => 1]);

        //dd(Sensorstream::first()->analyses->queues);

        $service = resolve(Sensorstreams::class);
        $service->addSensorstream($user, 'rest');
        $streams = $service->getSensorstreams($user);

        $this->assertEquals($stream->name, $streams->first()->name);
    }

    /** @test */
    public function it_can_sync_analysis_for_user()
    {
        $service = resolve(Sensorstreams::class);
        $stream = (new Sensorstream)->forceFill(['name' => 'rest', 'input' => 'interbeats', 'analyses' => ["queues" => "flow.rest"]]);
        $stream->save();
        $user = (new User)->forceFill(['id' => 1]);
        $clientService = (new ClientService)->forceFill(['id' => 1, 'name' => 'Rust monitor']);
        $service->addSensorstream($clientService, 'rest');

        $service->syncSensorstreams($user, $service->getSensorstreams($clientService));

        $this->assertEquals($clientService->sensorstreams->first()->name, $user->sensorstreams->first()->name);
    }

    /** @test */
    public function it_can_get_first_queue_element()
    {
        $stream = (new Sensorstream)->forceFill(['name' => 'rest', 'input' => 'interbeats', 'analyses' => ["queues" => ["flow", "rest"]]]);
        $stream->save();
        $user = (new User)->forceFill(['id' => 1]);

        $this->assertEquals($stream->analyses->queues[0], 'flow');
    }

    /** @test */
    public function it_can_get_head_queues()
    {
        $stream1 = (new Sensorstream)->forceFill(['name' => 'rest1', 'input' => 'interbeats', 'analyses' => ["queues" => ["flow", "rest"]]]);
        $stream1->save();
        $stream2 = (new Sensorstream)->forceFill(['name' => 'rest2', 'input' => 'interbeats', 'analyses' => ["queues" => ["flow", "strain"]]]);
        $stream2->save();
        $user = (new User)->forceFill(['id' => 1]);

        $service = resolve(Sensorstreams::class);
        $service->addSensorstream($user, 'rest1');
        $service->addSensorstream($user, 'rest2');
        $head = $service->getHeadQueues($user);

        $this->assertEquals($stream1->analyses->queues[0], $head['interbeats']);
        $this->assertCount(1, $head);
    }

    /** @test */
    public function it_can_get_tail_queues()
    {
        $stream1 = (new Sensorstream)->forceFill(['name' => 'rest1', 'input' => 'interbeats', 'analyses' => ["queues" => ["flow", "rest"]]]);
        $stream1->save();
        $stream2 = (new Sensorstream)->forceFill(['name' => 'rest2', 'input' => 'interbeats', 'analyses' => ["queues" => ["flow", "strain"]]]);
        $stream2->save();
        $user = (new User)->forceFill(['id' => 1]);

        $service = resolve(Sensorstreams::class);
        $service->addSensorstream($user, 'rest1');
        $service->addSensorstream($user, 'rest2');
        $tail = $service->getTailQueues($user);

        $this->assertEquals($stream1->analyses->queues[1], $tail['interbeats'][0]);
        $this->assertCount(1, $tail);
        $this->assertCount(2, $tail['interbeats']);
    }

    /** @test */
    public function it_can_get_cached_tail_queues()
    {
        $user = (new User)->forceFill(['id' => 1]);
        Cache::shouldReceive('remember')
            ->once()
            ->andReturn('tail');

        $service = resolve('Sensorstreams'); // => uses cachedsensorstreams, as defined in service provider
        $tail = $service->getTailQueues($user);

        $this->assertEquals('tail', $tail);
    }

}

class User extends Illuminate\Database\Eloquent\Model
{
    use Sensorium\Sensorstreams\HasSensorstreams;
}

class ClientService extends Illuminate\Database\Eloquent\Model
{
    protected $table = 'client_services';
    use Sensorium\Sensorstreams\HasSensorstreams;
}