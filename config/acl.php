<?php

return [
	// Define resources allowed for permission definition
	'resources' => [
		'Medication'
	],
	// Define which attribute contains the role key (obtained from external source)
	'role' => [
		'ref_name' => 'id'
	]
];