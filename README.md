# Sensorstreams

Service that can retrieve sensor analyses streams for an entity that uses the trait HasSensorstreams.
When user posts sensordata, this service can be used to retrieve the analysis pipeline of the user's sensordata. The pipeline defines an array of consecutive (kafka) queues used to stage sensordata (and intermediate results) for (spark) analysis

### Installing

* composer require sensorium/sensorstreams:dev-master
* Add HasSensorstreams trait to entities that have sensorstreams (User has sensorstreams, ClientService defines sensorstreams for a specific environment)
* Add ServiceProvider
* Check boot method in Serviceprovider. Migrations should only be performed by a single (master) instance if this module is used for multiple instances sharing a single database.